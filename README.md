# AIED Project
## Initial set up
We're using the 2017 ASSISTments dataset from https://sites.google.com/view/assistmentsdatamining/dataset.
This is hosted by me on gdrive here: https://drive.google.com/file/d/1TrPc-txq28vqiygG0FRbYoUg7wX_EAPp/view?usp=sharing. 

Find a description of features here: https://docs.google.com/spreadsheets/d/1QVUStXiRerWbH1X0P11rJ5IsuU2Xutu60D1SjpmTMlk/edit#gid=0.

You can either download it straight from the drive and rename it `assistments_2017.csv`, or run the following commands:

`pip install gdown`

`gdown -O assistments_2017.csv --id 1TrPc-txq28vqiygG0FRbYoUg7wX_EAPp` 

Alternatively, if you are on Euler:

`module load gcc/6.3.0 python/3.8.5`

`pip install --user gdown`

`gdown -O assistments_2017.csv --id 1QVUStXiRerWbH1X0P11rJ5IsuU2Xutu60D1SjpmTMlk`

## How to reproduce results
`pip install -r requirements.txt`

Most of our result code is using a notebook, and it is well commented. For a brief overview:
- `Clustering.ipynb` is the main notebook and can be use to run the grid search
- `Results.ipynb` is used to open the grid search CSV log and explore the clusterings
- `dateset_explore.ipynb` was used to generate some insights initially on the dataset
- `cluster_methods.py` includes several clustering methods
- `utils/dataloader.py` includes the ASSISTments dataloading code
- `utils/transforms.py` and `utils/features.py` include dataframe transforms and feature extraction code to be used in our dataset pipeline
