# Feature Ideas
https://docs.google.com/spreadsheets/d/1QVUStXiRerWbH1X0P11rJ5IsuU2Xutu60D1SjpmTMlk/edit#gid=0

Here are a list of columns we've kept and how we can turn them into student level features:
- Bolded features we can use as is
- Italicised features we can use if we take it from the entry with the latest timestamp

1. **StudentId**
1. **AveKnow**
1. **AveCarelessness**
1. **AveCorrect**
1. **NumActions**
1. **AveResBored**
1. **AveResEngcon**
1. **AveResConf**
1. **AveResFrust**
1. **AveResOfftask**
1. **AveResGaming**
1. skill: use for agg
1. problemId: use for agg
1. assignmentId: use for agg?
1. assistmentId: use for agg?
1. startTime: use for other features
1. timeTaken: average over student_id and maybe each KC
1. correct: average correct over each KC, or maybe some sort of TFIDF weighted score if they get correct for problems that are less frequently correct
1. original: avg correct given original
1. hintTotal: sum per problem for each student (only count once per problem id)
1. scaffold: avg correct given scaffold
1. bottomHint: total number of times student has used bottom out hint
1. _attemptCount_
1. problemType: use for agg
1. frIsHelpRequest: sum over student (maybe once per problem)
1. stlHintUsed: same as bottomHint
1. _totalFrPercentPastWrong_: this one is weird, either we take this for every KC or we find some way to quantify 'improvement', otherwise this is useless
1. _totalFrPastWrongCount_: see above
1. _totalFrTimeOnSkill_: maybe some score which relates total time on KC with avg correctness per KC
1. timeSinceSkill: some possible feature like the max timeSinceSkill while still being correct, but overall doesn't seem that useful.
1. totalFrAttempted: not that useful as overlaps with NumActions, **drop**
1. totalFrSkillOpportunities: **drop?**
1. responseIsFillIn: avg correctness given this
1. responseIsChosen: avg correctness given this
1. totalFrSkillOpportunitiesByScaffolding: **drop?**
1. timeGreater5Secprev2wrong: sum over student
1. helpAccessUnder2Sec: sum over student
1. timeGreater10SecAndNextActionRight: sum over student
1. **consecutiveErrorsInRow**
1. **totalTimeByPercentCorrectForskill**
1. timeOver80: sum over student
1. **schoolID**
1. 
1. MCAS

A lot of these features will have to be normalized over the number of problems/actions also

## Feature Set Split
Agg per student: 17 22 25 36 37 38 41
Avg correctness given x: 19 21 33 34 (maybe also some)
Italicised
Bolded
