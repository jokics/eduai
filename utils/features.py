""" 
Classes which define 'features' which can be modularly
appended to the sdets DataFrame.
"""
from abc import ABC, abstractmethod

import numpy as np
import pandas as pd
    
    
class FeatureSet(ABC):
    """
    Abstract class defining a set of features (or just one).
    """
    @abstractmethod
    def extract(self, df, raw_df):
        """ 
        Returns a new pd.DataFrame instance representing the new features.
        
        Caller should join the DataFrames together.
        """
        pass

# Features for new 2017 assistments data set
class StudentFeatures(FeatureSet):
    """
    Extracts features unique to each student (i.e. they're identical across all actions per student) from the
    raw table
    """
    
    def extract(self, df, raw_df):
        tmp = raw_df.drop_duplicates(subset=['studentId'])
        
        df_features = tmp[['studentId', 'InferredGender', 'AveKnow', 'AveCarelessness', 'AveCorrect', 'NumActions',
                          'AveResBored', 'AveResEngcon', 'AveResConf', 'AveResFrust', 'AveResOfftask',
                          'AveResGaming', 'totalTimeByPercentCorrectForskill', 'MiddleSchoolId']]
        
        df_features = df_features.set_index('studentId')
        
        return df_features
        

class AggStudentFeatures(FeatureSet):
    """
    Creates new features from the raw table by aggregating over student_id
    The following features are aggregated:
        - timeTaken
        - bottomHint
        - frIsHelpRequest
        - timeGreater5Secprev2wrong
        - helpAccessUnder2Sec
        - timeGreater10SecAndNextActionRight
        - timeOver80
        - responseIsFillIn
    """
    
    def extract(self, df, raw_df):
        # First, group actions by student
        actions_per_student = raw_df.groupby('studentId')
        
        # Next, aggregate the features
        avg_timeTaken = actions_per_student['timeTaken'].mean()
        avg_bottomHint = actions_per_student['bottomHint'].mean()
        avg_frIsHelpRequest = actions_per_student['frIsHelpRequest'].mean()
        avg_timeGreater5Secprev2wrong = actions_per_student['timeGreater5Secprev2wrong'].mean()
        avg_responseIsFillIn = actions_per_student['responseIsFillIn'].mean()
        avg_helpAccessUnder2Sec = actions_per_student['helpAccessUnder2Sec'].mean()
        avg_timeGreater10SecAndNextActionRight = actions_per_student['timeGreater10SecAndNextActionRight'].mean()
        avg_timeOver80 = actions_per_student['timeOver80'].mean()
        
        df_features = pd.concat([avg_timeTaken, avg_bottomHint, avg_frIsHelpRequest, 
                                 avg_timeGreater5Secprev2wrong, avg_responseIsFillIn,
                                 avg_helpAccessUnder2Sec, 
                                 avg_timeGreater10SecAndNextActionRight, 
                                 avg_timeOver80],
                       axis = 1)
        df_features.columns = ['AveTimeTaken', 'AveBottomHint', 'AveFrIsHelpRequest', 
                               'AveTimeGreater5SecPrev2Wrong', 'AveResponseIsFillIn',
                               'AveHelpAccessUnder2Sec',
                               'AveTimeGreater10SecAndNextActionRight', 'AveTimeOver80']

        return df_features
        
class AvgCorrectFeatures(FeatureSet):
    """
    Creates new features from the raw table by computing the average correctness given each of the
    following binary features:
        - original
        - scaffold
        
    i.e. for each binary feature x compute #actions that are correct and satisfy x (x = 1) divded by
    the total number of actions that satisfy x
    """
    
    def extract(self, df, raw_df):
        def get_correct_ratio(df):
            vc = df['correct'].value_counts()

            if 1 not in vc:
                return 0

            return vc[1] / vc.sum()

        ac_original = raw_df[raw_df['original'] == 1].groupby(['studentId']).apply(get_correct_ratio)
        ac_scaffold = raw_df[raw_df['scaffold'] == 1].groupby(['studentId']).apply(get_correct_ratio)

        df_features = pd.DataFrame(raw_df['studentId'].drop_duplicates())
        df_features = df_features.set_index('studentId')
        df_features = df_features.join(pd.DataFrame(ac_original, columns=['AveCorrectOriginal']), how='outer')
        df_features = df_features.join(pd.DataFrame(ac_scaffold, columns=['AveCorrectScaffold']), how='outer')
        
        return df_features
    
    
class LastActionStudentFeatures(FeatureSet):
    """
    Some student features are logged in terms of 'value X so far'. 
        - hintCount
        - attemptCount
        - totalFrPastWrongCount
        - totalFrAttempted
        
    We should find the 'last action' for each student and then get the value of this feature
    """
    def extract(self, df, raw_df):
        features = ['hintCount', 'attemptCount', 'totalFrPastWrongCount', 'totalFrAttempted']
        return raw_df.groupby('studentId').max('startTime')[features]
        
    
# Features for previous 2019 assistments data set
# class FractionFeatures(FeatureSet):
#     """
#     Creates an additional set of features by computing fractions of counts (e.g. fraction of number of started problem sets 
#     to number of assigned problem sets)
#     """
    
#     def extract(self, df):
#         # for the computed fractions below, division by 0 will result in infinite values and 0/0 will result in
#         # NaNs, so replace them by 0
        
#         # problem sets
#         fraction_of_started_problem_sets = (df['started_problem_sets_count']/df['problem_sets_assigned']).replace(
#             [np.inf, -np.inf], 0.0).fillna(0.0)
#         fraction_of_completed_problem_sets = (df['completed_problem_sets_count']/df['problem_sets_assigned']).replace(
#             [np.inf, -np.inf], 0.0).fillna(0.0)
#         # skill builders
#         fraction_of_started_skill_builders = (df['started_skill_builders_count']/df['skill_builders_assigned']).replace(
#             [np.inf, -np.inf], 0.0).fillna(0.0)
#         fraction_of_mastered_skill_builders = (df['mastered_skill_builders_count']/df['skill_builders_assigned']).replace(
#             [np.inf, -np.inf], 0.0).fillna(0.0)
        
#         df_features = pd.concat([fraction_of_started_problem_sets, fraction_of_completed_problem_sets,
#                                           fraction_of_started_skill_builders, fraction_of_mastered_skill_builders],
#                                axis = 1)
#         df_features.columns = ['fraction_of_started_problem_sets', 'fraction_of_completed_problem_sets',
#                                'fraction_of_started_skill_builders', 'fraction_of_mastered_skill_builders']

#         return df_features

# class ProblemFeatures(FeatureSet):
#     """
#     Creates an additional set of features based on the problem logs table which provides information on each problem
#     solved by each student and the problem details table which provides more information on the problem itself
#     (such as problem type)
#     Assumes that the current dataframe has already been joined with plogs (problem logs table) on student_id and with
#     pdets (problem details table) on problem_id
#     """
    
#     def extract(self, df):
#         # before creating the features, some preprocessing is required
        
#         # convert the answer_before_tutoring feature from Boolean (True/False) to an integer (1/0)
#         df['answer_before_tutoring'] = df['answer_before_tutoring'].fillna(0.0).astype(int)
#         # convert the answer_given feature from Boolean (True/False) to an integer (1/0)
#         df['answer_given'] = df['answer_given'].fillna(0.0).astype(int)
#         # convert the problem_completed feature from Boolean (True/False) to an integer (1/0)
#         df['problem_completed'] = df['problem_completed'].fillna(0.0).astype(int)
#         # Fraction of hints used is a measure of what percent  of hints were viewed by the student when solving the problem. 
#         # For entries where the problem has no hints available, fraction_hints_used will be empty (i.e. NaN). 
#         # In this case, set it to 0
#         df['fraction_of_hints_used'] = df['fraction_of_hints_used'].fillna(0.0)
#         # the tutoring_types feature is a list of categorical values representing what kinds of tutoring strategies are 
#         # available for the problem, which can be NaN in case there is no tutoring strategy
#         # replace with 'None' type in that case
#         df['tutoring_types'] = df['tutoring_types'].apply(lambda x: ['None'] if pd.isnull(x) else x)
#         # replace the tutoring type column for each problem by columns indicating which tutoring type(s) the problem belongs to
#         tmp = df['tutoring_types'].apply(pd.Series.value_counts).fillna(0.0)
#         df = pd.concat([df.drop(columns = ['tutoring_types']), tmp], axis = 1)
        
                
#         # group the problems from plogs by students
#         problems_per_student = df.groupby('student_id')
        
#         problems_per_student_count = problems_per_student['problem_id'].count()
        
        
#         # Compute the fraction of attempts to answer the problem before requesting tutoring
#         fraction_answer_before_tutoring = problems_per_student['answer_before_tutoring'].sum()/problems_per_student_count
        
#         # Compute the average fraction of hints used across all problems per student
#         avg_fraction_hints_used = problems_per_student['fraction_of_hints_used'].mean()
        
#         # Some hints have been deleted from the database, in this case the fraction_of_hints_used can be greater than one, 
#         # because students had more hints available to them when they completed the assignment than when the data was collected
#         # In this case, set the fraction to 1.0 if it exceeds 1.0
#         avg_fraction_hints_used[avg_fraction_hints_used > 1.0] = 1.0
        
#         # Compute average number of attempts across problems
#         avg_attempt_count = problems_per_student['attempt_count'].mean()
        
#         # Compute the fraction of the number of times the answer to a problem is provided to the student 
#         fraction_answer_given = problems_per_student['answer_given'].sum()/problems_per_student_count
        
#         # Compute the fraction of completed problems
#         fraction_problem_completed = problems_per_student['problem_completed'].sum()/problems_per_student_count
        
#         # Compute the fraction of each problem type
#         problem_types_per_student  = df.groupby(['student_id', 'problem_type'])
#         count_problem_types_per_student = problems_types_per_student['problem_id'].count().unstack(level = 1)
#         fraction_problem_types_per_student = count_problem_types_per_student.divide(problems_per_student_count, axis = 0)
        
#         # Compute the fraction of each tutoring type (Explanation, Hint, Scaffold, None)
#         fraction_explanation_type_per_student = problems_per_student['Explanation'].count()/problems_per_student_count
#         fraction_hint_type_per_student = problems_per_student['Hint'].count()/problems_per_student_count
#         fraction_scaffold_type_per_student = problems_per_student['Scaffold'].count()/problems_per_student_count
#         fraction_none_type_per_student = problems_per_student['None'].count()/problems_per_student_count
        
#         df_features = pd.concat([fraction_answer_before_tutoring, avg_fraction_hints_used, avg_attempt_count, 
#                                  fraction_answer_given, fraction_problem_completed,
#                                  fraction_problem_types_per_student, fraction_explanation_type_per_student,
#                                  fraction_hint_type_per_student, fraction_scaffold_type_per_student,
#                                  fraction_none_type_per_student],
#                                axis = 1) 
        
#         df_features.columns = ['fraction_answers_before_tutoring', 'avg_fraction_hints_used', 'avg_problem_attempt_count',
#                               'fraction_answer_given', 'fraction_problem_completed', 'fraction_problem_type_number',
#                               'fraction_problem_type_numeric', 'fraction_problem_type_exact_frac', 'fraction_problem_type_algebraic',
#                               'fraction_problem_type_mcq', 'fraction_problem_type_check', 'fraction_problem_type_external',
#                               'fraction_problem_type_eact_match_ignore_case', 'fraction_problem_type_exact_match_case_sensitive',
#                               'fraction_problem_type_open_response', 'fraction_problem_type_ordering',
#                               'fraction_tutoring_type_explanation', 'fraction_tutoring_type_hint',
#                               'fraction_tutoring_type_scaffold', 'fraction_tutoring_type_none']
        
#         return df_features  
