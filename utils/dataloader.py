""" Code to load dataset provide framework for pre-processing/transformations """

import os

import numpy as np
import pandas as pd

from utils.transforms import Transform
from utils.features import FeatureSet
from sklearn.preprocessing import StandardScaler


FEATURES_TO_DROP = [
    'SY ASSISTments Usage',
    'action_num',
    'assistmentId',
    'endTime',
    'hint',
    'frPast5HelpRequest',
    'frPast8HelpRequest',
    'past8BottomOut',
    'frPast5WrongCount',
    'frPast8WrongCount',
    'frWorkingInSchool',
    'endsWithScaffolding',
    'endsWithAutoScaffolding',
    'frTimeTakenOnScaffolding',
    'frTotalSkillOpportunitiesScaffolding',
    'frIsHelpRequestScaffolding',
    'sumRight',
    'sumTime3SDWhen3RowRight',
    'sumTimePerSkill',
    'Prev5count',
    'manywrong',
    'confidence(BORED)', 
    'confidence(CONCENTRATING)',
    'confidence(CONFUSED)', 
    'confidence(FRUSTRATED)',
    'confidence(OFF TASK)', 
    'confidence(GAMING)',
    'RES_BORED',
    'RES_CONCENTRATING', 
    'RES_CONFUSED', 
    'RES_FRUSTRATED', 
    'RES_OFFTASK',
    'RES_GAMING',
    'Ln-1',
    'Ln',
]


class Assistments:
    """ Class to represent the 2017 ASSISTments dataset """
    hidden = {}
    
    def __init__(self, path):
        """ 
        Load csv as a pandas dataframe, and initialise an empty
        dataframe with the student ID as the index and the student MCAS
        as its only column. Perform some initial data cleaning
        """
        self.raw_data = pd.read_csv(path, header=0) 
        
        print(f'Total number of actions: {len(self.raw_data)}')
        print(f'Total unique students: {self.raw_data["studentId"].nunique()}')
        
        # Drop some unusable columns
        self.raw_data.drop(columns = FEATURES_TO_DROP, inplace=True)
        self.raw_data = self.raw_data[self.raw_data['MCAS'] != -999]
        
        print('Total unique students after MCAS clean:',
              f'{self.raw_data["studentId"].nunique()}')
        
        # Retrieve a new dataframe with studentID as an index
        self.data = self.raw_data.drop_duplicates(subset='studentId')[['studentId', 'MCAS']]
        self.data = self.data.set_index('studentId')
        
                        
    def apply(self, steps):
        """
        Apply a list of steps (either Transform or FeatureSet instances)
        to self.data.
        
        Modifies the dataframe in place, and returns nothing.
        
        Return to utils.transforms and utils.features for more info...
        """
        print(f'Applying {len(steps)} steps:')
        
        for i, step in enumerate(steps):
            # Check steps
            assert(isinstance(step, FeatureSet) or isinstance(step, Transform))
            
            # Apply relevant method
            if isinstance(step, Transform):
                self.data = step.apply(self.data)
            else:
                self.data = self.data.join(step.extract(self.data, 
                                                        self.raw_data))
                
            print(f'Step {i + 1}/{len(steps)}: {type(step)}')
            
            
    def hide_feature(self, name):
        """
        Remove feature with 'name' from self.data and store it
        as a dataframe (indexed by studentId) in the dictionary self.hidden.
        
        e.g. hide_feature('MCAS') 
             -> self.hidden = { 'MCAS': instance of pd.DataFrame }
        """
        if 'studentId' in self.data.columns:
            self.hidden[name] = self.data[['studentId', name]]
            self.hidden[name].set_index('studentId', inplace=True)
        else:
            # Assume student_id is the index
            self.hidden[name] = pd.DataFrame(self.data[name])
            self.hidden[name].index = self.data.index
        
        self.data = self.data.drop(columns = [name])
        