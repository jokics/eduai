""" 
Standard dataframe transform functions which are used as part of our
clustering pipeline
"""
from abc import ABC, abstractmethod

import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA


class Transform(ABC):
    """
    Abstract class defining a 'dataframe transform'
    """
    @abstractmethod
    def apply(self, df):
        """ Process a dataframe, return transformed dataframe """
        pass
    
    
class ApplyFunc(Transform):
    """
    Generic wrapper to pass a function that takes only the dataframe as a param
    and returns only the transformed dataframe.
    
    Here just in case you want to test something quick, use a lambda perhaps.
    """
    def __init__(self, func):
        self.func = func
        
    def apply(self, df):
        return self.func(df)
    
    
class DropDuplicates(Transform):
    """
    Wrapper for pd.DataFrame.drop_duplicates as a transform.
    """
    def __init__(self, column_name):
        self.column_name = column_name
        
    def apply(self, df):
        return df.drop_duplicates(subset = self.column_name, keep = 'first')
    
    
class Reindex(Transform):
    """
    Wrapper for pd.DataFrame.set_index as a transform.
    """
    def __init__(self, column_name):
        self.column_name = column_name
        
    def apply(self, df):
        return df.set_index(self.column_name)
    
    
class DropColumns(Transform):
    """
    Wrapper for pd.DataFrame.drop as a transform.
    """
    def __init__(self, column_names):
        self.column_names = column_names
        
    def apply(self, df):
        return df.drop(columns = self.column_names)
    
    
class DropWhere(Transform):
    """
    Drop all rows matching a lambda function
    """
    def __init__(self, predicate_func):
        self.fn = predicate_func
        
    def apply(self, df):
        return df[~df.apply(self.fn, axis=1)]
    

class Project(Transform):
    """
    Drop all columns _except_ the ones specified.
    """
    def __init__(self, column_names):
        self.column_names = column_names
        
    def apply(self, df):
        return df.drop(df.columns.difference(self.column_names))
      
        
class OneHotEncodeColumn(Transform):
    """
    Use pd.get_dummies to replace df column with one hot encoded column
    (dropping redundant column).
    """
    def __init__(self, column_name):
        self.column_name = column_name
        
    def apply(self, df):
        dummies = pd.get_dummies(df[self.column_name], prefix=self.column_name)
        ohe_cols = [col for col in dummies.columns if col.startswith(self.column_name)]
        
        dummies = dummies[ohe_cols]
        
        return df.drop(columns=[self.column_name]).join(dummies)
    
    
class Impute(Transform):
    """
    Use either sklearn.impute.SimpleImputer or KNNImputer to impute
    missing/nan values in the dataframe. 
    
    Pass the Imputer instance to the constructor of this function.
    """
    def __init__(self, imputer):
        self.imputer = imputer
        
    def apply(self, df):
        imputed_df = pd.DataFrame(self.imputer.fit_transform(df))
        imputed_df.columns = df.columns
        imputed_df.index = df.index
        
        return imputed_df
    
class Standardize(Transform):
    """
    Standardize selected features (columns) in the dataframe by subtracting the mean and dividing by the standard deviation
    """
    
    def apply(self, df):
        sc = StandardScaler()
        
        df[df.columns.tolist()] = sc.fit_transform(df[df.columns.tolist()])
        
        return df
    

class PCATransform(Transform):
    """
    Apply sklearn.decomposition.PCA fit_transform to the data.
    """
    def __init__(self, n_components, random_state):
        self.n_components = n_components
        self.random_state = random_state
        
    def apply(self, df):
        pca = PCA(n_components = self.n_components, random_state = self.random_state)
        pca_labels = [f'PC{i + 1}' for i in range(self.n_components)]
        df_pca = pd.DataFrame(data = pca.fit_transform(df), columns=['PC1', 'PC2'], index=df.index)
        
        print('Variance explained: ', pca.explained_variance_ratio_)
        
        return df_pca