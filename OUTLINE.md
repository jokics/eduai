# Outline

### introduction (write at end)

### background (related works)
- paper from silva and silva (o.g. paper) - https://ieeexplore.ieee.org/abstract/document/9274130
- clustering on assistments - http://educationaldatamining.org/EDM2017/proc_files/papers/paper_38.pdf
- CLUSTERING EDUCATIONAL DIGITAL LIBRARY USAGE DATA - https://jedm.educationaldatamining.org/index.php/JEDM/article/view/21
- Using a Latent Class Forest to Identify At-Risk Students in Higher Education - https://jedm.educationaldatamining.org/index.php/JEDM/article/view/283


### theoretical explanation of modeling techniques (clustering)
- clustering (general)
- k-means clustering ?
- hierarchical clustering?
- spectral clustering ?

### data 
- assistments set 
- pre-processing 
- 

### methodology (specific to us)
- grid search -> feature selection
- sillouhette
- adjusted rand index

### experimental findings

### analysis

link to google doc: https://docs.google.com/document/d/1cIeH49M-opX8Hdx-XEfH-YZMurGW_dszioF7Mz39HfA/edit?usp=sharing
