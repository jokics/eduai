""" Cluster method classes """

from abc import ABC, abstractmethod

from sklearn.cluster import KMeans, AgglomerativeClustering, SpectralClustering
from sklearn.mixture import GaussianMixture


class ClusterMethod(ABC):
    """
    Abstract class representing a cluster method. We want a unified interface
    s.t. any later methods can expect the same class methods regardless of 
    which library the cluster code comes from.
    
    Contains a kwargs member variable which can be passed to the clustering instance
    for any fine method specifications
    """
    name = ''
    
    def __init__(self, kwargs={}, name_override=None):
        self.kwargs = kwargs
        if name_override is not None:
            self.name = name_override
        
    @abstractmethod
    def cluster(self, data, n_clusters):
        """
        Abstract method which should perform clustering and 
        return the cluster labels. 
        """
        pass
    
    def __str__(self):
        return self.name
    
    
class KMeansClusterMethod(ClusterMethod):
    name = 'KMeans'
    
    def cluster(self, data, n_clusters):
        kmeans = KMeans(n_clusters = n_clusters, random_state = 33, **self.kwargs)
        return kmeans.fit_predict(data)        
    
    
class GMMClusterMethod(ClusterMethod):
    name = 'GMM'
    
    def cluster(self, data, n_clusters):
        gmm = GaussianMixture(n_components=n_clusters, random_state=33, **self.kwargs)
        return gmm.fit_predict(data)
    
    
class HierarchicalClusterMethod(ClusterMethod):
    name = 'HClust'
    
    def cluster(self, data, n_clusters):
        agg_clustering = AgglomerativeClustering(n_clusters = n_clusters, **self.kwargs)
        return agg_clustering.fit_predict(data)
    
    
class SpectralClusterMethod(ClusterMethod):
    name = 'Spectral'
    
    def cluster(self, data, n_clusters):    
        spec_clustering = SpectralClustering(n_clusters=n_clusters, affinity='nearest_neighbors', random_state=33, **self.kwargs)
        return spec_clustering.fit_predict(data)